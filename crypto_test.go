package crypto_test

import (
	"fmt"

	"bitbucket.org/pvelder/crypto"
)

func ExampleVerifyAndDecrypt() {

	textIn := "This is the original input"

	fromCertificateCer := "./mycert.pfx.cer.pem"
	fromCertificateKey := "./mycert.pfx"
	fromCertificatePassword := "My super password"
	toCertificateCer := "./Dep.pfx.cer.pem"
	toCertificateKey := "./Dep.pfx"
	toCertificatePassword := ""

	cryptoText, encryptedKey, encryptedIV, signature, err := crypto.EncryptAndSign(textIn, fromCertificateKey, fromCertificatePassword, toCertificateCer)
	if err != nil {
		panic(err)
	}

	// fmt.Printf("textin: %v, cryptotext: %v, encryptedKey: %v, encryptedIV: %v, signature: %v", textIn, cryptoText, encryptedKey, encryptedIV, signature)

	decrypted, err :=
		crypto.VerifyAndDecrypt(cryptoText, encryptedKey, encryptedIV, signature, toCertificateKey, toCertificatePassword, fromCertificateCer)
	if err != nil {
		panic(err)
	}

	fmt.Println(string(decrypted))
	// Output:  This is the original input

}
