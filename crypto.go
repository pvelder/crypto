package crypto

import (
	"crypto"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"io/ioutil"

	"github.com/pkg/errors"
	"golang.org/x/crypto/pkcs12"
)

func aesCbcEncrypt(key, iv, plainText []byte) ([]byte, error) {

	block, err := aes.NewCipher(key)

	if err != nil {
		return nil, err
	}

	blockSize := block.BlockSize()
	origData, err := PKCS7Pad(plainText, blockSize)
	if err != nil {
		return nil, errors.Wrapf(err, "can not PKCS7Pad, blocksize %v", blockSize)
	}
	blockMode := cipher.NewCBCEncrypter(block, iv)
	cryted := make([]byte, len(origData))
	blockMode.CryptBlocks(cryted, origData)
	return cryted, nil
}

func aesCbcDecrypt(key, iv, cipherText []byte) ([]byte, error) {

	block, err := aes.NewCipher(key)

	if err != nil {
		return nil, err
	}

	blockMode := cipher.NewCBCDecrypter(block, iv)
	origData := make([]byte, len(cipherText))
	blockMode.CryptBlocks(origData, cipherText)
	origData, err = PKCS7Unpad(origData, block.BlockSize())
	if err != nil {
		return nil, errors.Wrapf(err, "can not PKCS7Unpad, blocksize %v", block.BlockSize)
	}

	return origData, nil
}

func generateSequence(keylen int) ([]byte, error) {
	key := make([]byte, keylen)
	_, err := rand.Read(key)
	return key, err
}

func privateKey(keyFileName, keyPassword string) (*rsa.PrivateKey, error) {
	data, err := ioutil.ReadFile(keyFileName)
	if err != nil {
		return nil, errors.Wrapf(err, `Error reading private key file "%s"`, keyFileName)
	}

	priv, _, err := pkcs12.Decode(data, keyPassword)
	if err != nil {
		return nil, errors.Wrapf(err, `Error decoding private key file "%s"`, keyFileName)
	}

	if err := priv.(*rsa.PrivateKey).Validate(); err != nil {
		return nil, errors.Wrapf(err, `Error validating private key file "%s"`, keyFileName)
	}

	return priv.(*rsa.PrivateKey), nil
}

func publicKey(keyFileName string) (*rsa.PublicKey, error) {
	rootPEM, err := ioutil.ReadFile(keyFileName)
	if err != nil {
		return nil, errors.Wrapf(err, `Error reading public key file "%s"`, keyFileName)
	}
	block, _ := pem.Decode([]byte(rootPEM))
	var cert *x509.Certificate
	cert, err = x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, errors.Wrapf(err, `Error x509.ParseCertificate "%s"`, keyFileName)
	}
	rsaPublicKey := cert.PublicKey.(*rsa.PublicKey)
	return rsaPublicKey, nil
}

// EncryptAndSign will
// 1) AES encrypt the data using key and IV
// 2) RSA encrypt of AES key and IV
// 3) sign with RSA signature
//
// It returns the encrypted data, key and IV, and signature all base64 encoded.
func EncryptAndSign(plainText string, fromP12certificate string, fromP12CertPasswd string, toP12certificate string) (b64CryptoText, b64EncryptedKey, b64EncryptedIV, b64Signature string, err error) {
	key, err := generateSequence(32) // 32 = AES-256 keysize
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not generate key")
	}

	iv, err := generateSequence(aes.BlockSize) // 16 = aes.Blocksize
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not generate iv")
	}

	cryptoText, err := aesCbcEncrypt(key, iv, []byte(plainText))
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not AES CBC encrypt")
	}

	// encrypt key an initialization vector (iv) with RSA, public key with PKCS#1 1.5 padding
	priv, err := privateKey(fromP12certificate, fromP12CertPasswd)
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not get private key from p12 certificate %s", fromP12certificate)
	}

	toPubKey, err := publicKey(toP12certificate)
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not get public key from p12 certificate %s", toP12certificate)
	}

	encryptedKey, err := rsa.EncryptPKCS1v15(rand.Reader, toPubKey, key)
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not RSA encrypt key")
	}
	encryptedIV, err := rsa.EncryptPKCS1v15(rand.Reader, toPubKey, iv)
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not RSA encrypt iv")
	}

	// sign with SHA1 hashing and Pkcs1 padding
	hashed := sha1.Sum(cryptoText)
	signature, err := rsa.SignPKCS1v15(rand.Reader, priv, crypto.SHA1, hashed[:])
	if err != nil {
		return "", "", "", "", errors.Wrapf(err, "can not RSA sign (SHA1)")
	}

	return base64.StdEncoding.EncodeToString(cryptoText),
		base64.StdEncoding.EncodeToString(encryptedKey),
		base64.StdEncoding.EncodeToString(encryptedIV),
		base64.StdEncoding.EncodeToString(signature), nil
}

// VerifyAndDecrypt are the reverse action of EncryptAndSign.  It will
// 1) verify validatity of the RSA signature
// 2) RSA decrypt of AES key and IV
// 3) AES decrypt the data using key and IV
//
// All arguments are base64 encoded. It returns the decrypted data.
func VerifyAndDecrypt(b64CryptoText, b64EncryptedKey, b64EncryptedIV, b64Signature, toP12certificate, toP12CertPasswd, fromP12certificate string) ([]byte, error) {
	cryptoText, err := base64.StdEncoding.DecodeString(b64CryptoText)
	if err != nil {
		return nil, errors.Wrapf(err, `can not base64-decode b64CryptoText "%s"`, b64CryptoText)
	}

	encryptedKey, err := base64.StdEncoding.DecodeString(b64EncryptedKey)
	if err != nil {
		return nil, errors.Wrapf(err, `can not base64-decode b64EncryptedKey "%s"`, b64EncryptedKey)
	}

	encryptedIV, err := base64.StdEncoding.DecodeString(b64EncryptedIV)
	if err != nil {
		return nil, errors.Wrapf(err, `can not base64-decode b64EncryptedIV "%s"`, b64EncryptedIV)
	}

	signature, err := base64.StdEncoding.DecodeString(b64Signature)
	if err != nil {
		return nil, errors.Wrapf(err, `can not base64-decode b64Signature "%s"`, b64Signature)
	}

	priv, err := privateKey(toP12certificate, toP12CertPasswd)
	if err != nil {
		return nil, errors.Wrapf(err, "can not get private key from p12 certificate %s", toP12certificate)
	}
	fromPubKey, err := publicKey(fromP12certificate)
	if err != nil {
		return nil, errors.Wrapf(err, "can not get public key from p12 certificate %s", fromP12certificate)
	}

	// verify signature with SHA1 hashing and Pkcs1 padding
	hashed := sha1.Sum(cryptoText)
	err = rsa.VerifyPKCS1v15(fromPubKey, crypto.SHA1, hashed[:], signature)
	if err != nil {
		return nil, errors.Wrapf(err, "can not RSA verify signature (SHA1)")
	}

	// decrypt key and IV
	key, err := rsa.DecryptPKCS1v15(rand.Reader, priv, encryptedKey)
	if err != nil {
		return nil, errors.Wrapf(err, "can not RSA decrypt key")
	}
	iv, err := rsa.DecryptPKCS1v15(rand.Reader, priv, encryptedIV)
	if err != nil {
		return nil, errors.Wrapf(err, "can not RSA decrypt iv")
	}

	// decrypt the data using key and iv
	decryptedText, err := aesCbcDecrypt(key, iv, cryptoText)
	if err != nil {
		return nil, errors.Wrapf(err, "can not AES decrypt")
	}

	return decryptedText, nil
}
